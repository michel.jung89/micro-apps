# dGraph

## Start

```bash
# start zero1, alpha1 and ratel
docker-compose -f docker-compose-dgraph.yml up
# check 
docker-compose -f docker-compose-dgraph.yml ps
# stop all
docker-compose -f docker-compose-dgraph.yml down
```
